;Danny Vasquez
;Realizado el 22 de julio 2020

%macro imprimir 2
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro

%macro leer 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro


section .data

    ingValor db "Ingrese el valor: "
    leningValor EQU $-ingValor

    mensajeResultado db "El valor ingresado es: ",10
    lenmensajeResultado EQU  $-mensajeResultado
    
    nlinea		db		10,10,0
	lnlinea		equ		$ - nlinea

    asterisco db '*'

    
section .bss
  	valor:		resb 1

section .text
    global _start    

_start:
    

    mov eax, 5
    ;mov ebx, 1
    ;add eax, 1
    add eax, '0'
    mov [valor], eax
    imprimir mensajeResultado, lenmensajeResultado
    imprimir valor, 2


    


saltoLinea:
    mov eax, 4
	mov ebx, 1
	mov ecx, nlinea
	mov edx, lnlinea
	int 80h      


salir:
    mov eax, 1
    int 80h   