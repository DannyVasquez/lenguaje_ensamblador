;Programa que imprime

%macro imprimir 2
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro


section .data

    nlinea		db		10,0
	lnlinea		equ		$ - nlinea

section .bss
    contador: resb 1

section .text
    global _start    

_start:

    mov edx, 9
    mov [contador], edx


imprimirBloque: 

    ;Imprime y controla el contador
    mov edx,[contador]
    dec edx
    add edx, '0'
    mov [contador], edx ;Transformo a digito para imprimir
    imprimir contador, 1

    ;Asigno nuevamente el nuevo valor del contador
    mov edx,[contador] 
    sub edx, '0'
    mov [contador], edx ;Pasa a ser digito nuevamente
    
    ;Comparo el valor del contador si llega a ser 0, para se realice un salto de linea 
    cmp edx, 0
    jnz saltoLinea
    jz salir
    


 
saltoLinea:
    imprimir nlinea, lnlinea

    jmp imprimirBloque   
salir:
    imprimir nlinea, lnlinea

    mov eax, 1
    int 80h