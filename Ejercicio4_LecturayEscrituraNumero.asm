section .data
    ;Poner en orden la variable y la longitud
       mensaje DB "Ingrese un numero", 10 ; el 10 decir q vamos a concatenar un 10
       len_mensaje EQU $-mensaje 

       mensaje_presentacion DB "El numero ingresado es", 10
       len_mensaje_presentacion EQU $-mensaje_presentacion 

section .bss 
        numero resb 5

section .text
         global _start
_start:

        ;*******************IMPRIME MENSAJE
        mov eax, 4 ;Puede tomar valroes de 1 hasta 4
        mov ebx, 1
        mov ecx, mensaje 
        mov edx, len_mensaje
        int 80H

        ;*******************LECTURA DE NUMERO
        mov eax, 3 ;
        mov ebx, 2
        mov ecx, numero 
        mov edx, 5
        int 80H

        ;*******************IMPRIME MENSAJE PRESENTACION
        mov eax, 4 ;
        mov ebx, 1
        mov ecx, mensaje_presentacion 
        mov edx, len_mensaje_presentacion
        int 80H

        ;*******************IMPRIMIR NUMERO
        mov eax, 4 ;
        mov ebx, 1
        mov ecx, numero 
        mov edx, 5
        int 80H

        ;*******************   SALIDA DEL PROGRAMA
        mov eax, 1          ; salida cuando tenemos la interrupcion, salida del programa_exit, sys_exit
        int 80H