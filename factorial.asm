%macro leer 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section	.data
	mensa_Entrada db "Ingrese el valor para calcular el factorial:"
	len_mensa_Entrada equ $ - mensa_Entrada

	mensa_Fact db "Factorial es: "	
	len_mensa_Fact equ $ - mensa_Fact	
	
	nlinea		db		10,0
	lnlinea		equ		$ - nlinea	

section .bss
	valor resb 5
	factorial resb 5
	
section	.text
   global _start        
	
_start:                 

   ;mov eax, 4            
   ;mov ebx, 1
   ;mov ecx, eax
  	
   imprimir mensa_Entrada,len_mensa_Entrada	 
   leer valor, 5        ;Factorial de un numero ingresado

   mov ax, [valor]
   sub ax, '0'
   mov bx, 1
   mov cx, ax


facto:
   
   mul bl                           ;ax = ax * bx =>3 x 1 =3	
   mov [factorial], al					
   dec cl ;2
   mov al, cl
   mov bl, [factorial]
   cmp cl, 0
   jg facto
   
   jmp imprimir22
   
imprimir22:

   add ebx, '0'
   mov [factorial], ebx

   imprimir mensa_Fact,len_mensa_Fact
   imprimir factorial,5
   imprimir nlinea, lnlinea

salir:
   mov	  eax,1        
   int	  80h
