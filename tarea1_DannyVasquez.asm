;Nombre:Danny Vasquez
;Ciclo: 6to "A"

;Comentar el significado de cada línea

%macro imprimir 2 ; Declaramos el macro con el nombre "imprimir" con 2 parametros a recibir. 
	mov eax,4 	; Subrutina de escritura
	mov ebx,1	; Estandar(1 hace referencia a la salida).
	mov ecx,%1 	; %1 se refiere al primer parámetro macro que lo desplaza a al registro ecx.
	mov edx,%2	; Se mueve el segundo parametro al registro edx, que hace la referencia a longitud de la cadena a imprimir.    
	int 80H		; Interrupcion para imprimir en pantalla.
%endmacro ; Termina el macro

%macro leer 2  ; Declaramos el macro con el nombre "leer" con 2 parametros a recibir. 
    mov eax,3  ; Subrutina de lectura.
    mov ebx,0  ; Estandar(0 hace referencia a la entrada).
    mov ecx,%1 ; %1 se refiere al primer parámetro macro que lo desplaza a al registro ecx.
    mov edx,%2 ; Se mueve el segundo parametro al registro edx, que hace la referencia a longitud de la cadena a imprimir.
    int 80H  ; Interrupcion para imprimir en pantalla.
%endmacro ; Termina el macro


; ecx,modo de acceso
; edx, permisos
section .bss ; Seccion para declarar variables
	auxiliar resb 30 ; Establecemos 30 espacios de memoria en ka variable auxiliar
	auxiliarb resb 30 ; Establecemos 30 espacios de memoria en ka variable auxiliarb
	auxiliarc resb 30 ; Establecemos 30 espacios de memoria en ka variable auxiliarc


section .data ;Seccion para declarar constantes
	msg db 0x1b  ,"       " ; 6 espacios para contener al dato.
	lenmsg equ $-msg ; Calcula el numero de caracteres o longitud de la constante msg.



	salto db " ",10 ; Declaramos una constante y se concatena 10 espacios a la misma constantes "salto".
	lensalto equ $-salto ; Calcula el numero de caracteres o longitud de la constante "salto" que en este caso son espacios.




section .text ; Sección donde comienza el código.
    global _start ;Apunta a una dirección de memoria.    

_start: ; Es el punto de entrada, marca la primera instrucción a ser ejecutada.
	
	mov ecx,9 ; Asignamos al registro contador el valor de 9 para que LOOP genere 9 ciclos en el "cicloI".

	mov al,0 ; Mueve el 0 decimal a la subregistro al.
	mov [auxiliar],al ; Copia "al" en el byte en "auxiliar"

cicloI: ;Inicio de un ciclo llamado "cicloI"
	push ecx  ; El valor de ecx lo ubica en la pila 	
	mov ecx,9 ; Asignamos nuevamente al registro contador el valor de 9 para que LOOP genere 9 ciclos en el "cicloJ".

	mov al,0 ; Mueve el 0 decimal a la subregistro al.
	mov [auxiliarb],al ; Copia "al" en el byte en "auxiliarb"

	cicloJ: ;Inicio de un ciclo llamado "cicloJ".
		push ecx ; El valor de ecx lo ubica en la pila.  


		call imprimir0al9 ; Llama a la función llamada "imprimir0al9".
		

	;	imprimir msg2,lenmsg2

	fincicloJ: ;Fin del ciclo llamado "cicloJ"
		mov al,[auxiliarb] ; Copia el byte que se encuentra "auxiliarb" en "al".
		inc al ;Incrementa en uno el valor de "al".
		mov [auxiliarb],al ; Copia "al" en el byte en "auxiliarb".

		pop ecx ; Saca el ultimo valor de la pila y lo ubica en el registro "ecx".  
		loop cicloJ ;Cierra el ciclo llamado "cicloJ".
		
	;imprimir salto,lensalto

fincicloI: ;Fin del ciclo llamado "cicloI"
	mov al,[auxiliar] 	; Copia el byte que se encuentra "auxiliar" en "al".
	inc al				; Incrementa en uno el valor de "al".
	mov [auxiliar],al	; Copia "al" en el byte en "auxiliar".

	pop ecx ; Saca el ultimo valor de la pila y lo ubica en el registro "ecx".  
	loop cicloI ; Cierra el ciclo llamada "cicloI". 
	

salir: ;Zona para cerrar el programa
	mov eax, 1 ; Subrutina de salida del programa(sys_exit).
	int 80H	   ; Interrupcion para imprimir en pantalla.



imprimir0al9: ;Declaración de una funcion
	
	mov ebx,"[" ; Almacena el valor correspondiente del caracter "[" del codigo ASCII en la dirección "ebx". 
	mov [msg+1], ebx ; Mueve el valor en "ebx" a la ubicación de la memoria una vez mas allá de la ubicación señalada por la variable "msg".

	mov bl,[auxiliar]	 ; Copio el byte que se encuentra "auxiliar" en "bl".
	add bl,'0'			; bl=bl+'0', es decir concatena el caracter '0' a "bl".
	mov [msg+2], bl ; Mueve el valor en "bl" a la ubicación de la memoria dos veces mas allá de la ubicación señalada por la variable "msg".


	mov ebx,";"		 ; Almacena el valor correspondiente del caracter ";" del codigo ASCII en la dirección "ebx". 
	mov [msg+3], ebx ; Mueve el valor en "ebx" a la ubicación de la memoria tres veces mas allá de la ubicación señalada por la variable "msg".

	
	mov bl,[auxiliarb]	; Copio el byte que se encuentra "auxiliarb" en "bl".
	add bl,'0'			; bl=bl+'0', es decir concatena el caracter '0' a "bl".
	mov [msg+4],bl		; Mueve el valor en "bl" a la ubicación de la memoria cuatro veces mas allá de la ubicación señalada por la variable "msg".

	mov ebx,"fJ"		; Almacena el valor "fJ" en la dirección "ebx". 
	mov [msg+5], ebx	; Mueve el valor en "ebx" a la ubicación de la memoria cinco veces mas allá de la ubicación señalada por la variable "msg".

	imprimir msg,lenmsg	; Utilizamos el macro imprimir enviando como argumentos el valor de "msg" y "lenmsg". 

	ret	;Retorno de la funcion.
