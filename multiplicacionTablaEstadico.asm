
%macro imprimir 2
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro

section .data
    msj1 db '*'
    len_msj1 equ $ - msj1

    msj2 db '='
    len_msj2 equ $ - msj2

    nlinea		db		10,0
	lnlinea		equ		$ - nlinea

section .bss
    a resb 2
    b resb 2
    c resb 2

section .text
    global _start

_start:

mov al, 1
principal:

    ;push rax

    mov rax, 3
    add rax, '0'
    mov [a] , rax
    mov rcx, 1

    ;pop ral
    ;inc ral
    ;cmp ral, 9
    ;jnz principal
    ;jmp salir

ciclo:


    push rcx
    
    mov ax, [a] 
    sub ax, '0'    
    mul cx
    add ax, '0' 

    
    add cx, '0'
    mov [b], cx
    mov [c], ax ;Valor de la multiplicacion





    imprimir a, 1 ;Imprime 1er factor
    imprimir msj1, len_msj1 ;Imprime el signo
    imprimir b, 1 ;Imprime 2do factor
    imprimir msj2, len_msj2 ;Imprime el igual
    imprimir c, 1   ;Imprime el resultado
    imprimir nlinea, lnlinea


    pop rcx
    inc cx
    cmp cx, 10
    jnz ciclo

salir:    
    mov eax, 1
    int 80h
