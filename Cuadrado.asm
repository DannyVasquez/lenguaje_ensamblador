;Danny Vasquez
;Realizado el 22 de julio 2020

%macro imprimir 2
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro


section .data
    asterisco db '*'


    nlinea		db		10,0
	lnlinea		equ		$ - nlinea

section .bss
    contador: resb 1

section .text
    global _start    

_start:
    
    mov cx, 9 ;Valor para imprimir los asteriscos en una fila

    mov edx, 9
    mov [contador], edx ;numero de filas a imprimir

imprimirBloque: ;etiqueta  

    ;Fila de asteriscos
    dec cx 
    push cx  
    imprimir asterisco, 1
    pop cx
    cmp cx,0
    jnz imprimirBloque
    
    
    ;Imprime y controla el contador
    mov edx,[contador]
    dec edx
    add edx, '0'
    mov [contador], edx ;Transformo a cadena para imprimir
    imprimir contador, 1
    
    ;Asigno nuevamente el nuevo valor del contador
    mov edx,[contador] 
    sub edx, '0'
    mov [contador], edx ;Pasa a ser digito nuevamente

    ;Comparo el valor del contador si llega a ser 0, para se realice un salto de linea 
    cmp edx, 0
    jnz saltoLinea
    jz salir


    
saltoLinea:

    imprimir nlinea, lnlinea
    
    mov cx, 9 ;Valor q asigna nuevamente para imprimir nueva fila de asteriscos
    jmp imprimirBloque    

salir:
    imprimir nlinea, lnlinea

    mov eax, 1
    int 80h