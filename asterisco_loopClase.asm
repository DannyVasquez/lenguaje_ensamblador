;Realizado por Danny Vasquez
%macro imprimir 2
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro

section .data
    
    asterisco db '*'

    nlinea		db		10,0
	lnlinea		equ		$ - nlinea

section .text
    global _start    

_start:

    mov rcx, 8
    mov rbx, 1

l1:

    push rcx
    push rbx

    mov rcx, rbx ; Contador solo esta incrementanndo

    ;pop rcx  ;estas 2 partes se quitaron con el mov
    ;push rcx


l2:
    push rcx
    imprimir asterisco, 1

    ;filaliza loop de las columnas
    pop rcx 
    loop l2 ;dec cx

    imprimir nlinea, lnlinea ;nueva lìnea


    ;finaliza loop de las filas
    pop rbx
    pop rcx
    inc rbx
    loop l1

salir:
    ;imprimir nlinea, lnlinea ;nueva lìnea

    mov eax, 1
    int 80h      