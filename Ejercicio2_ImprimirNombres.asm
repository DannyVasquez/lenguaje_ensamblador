section .data
    ;Poner en orden la variable y la longitud
       nombresApe DB "Danny Vinicio Vasquez Calderon", 10 ; el 10 decir q vamos a concatenar un 10
       lonNomb EQU $-nombresApe 

       materia DB "Lenguaje Ensamblador", 10
       lonMat EQU $-materia 

       genero DB "Masculino", 10
       lonGenero EQU $-genero 

section .text
         global _start
_start:

    mov eax, 4 ;Puede tomar valroes de 1 hasta 4
    mov ebx, 1
    mov ecx, nombresApe 
    mov edx, lonNomb
    int 80H

    mov eax, 4
    mov ebx, 1
    mov ecx, materia 
    mov edx, lonMat
    int 80H

    mov eax, 4
    mov ebx, 1
    mov ecx, genero 
    mov edx, lonGenero
    int 80H



    ; PARA CERRAR EL PROGRAMA
    mov eax, 1          ; salida cuando tenemos la interrupcion, salida del programa_exit, sys_exit
    mov ebx, 0          ; si el retorno es 0(200 en la web) ok      normalmente son movimientos pero cambia cuando le das una interrupcion
    int 80H
        