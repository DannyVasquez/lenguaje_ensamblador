section .data
	mensaje db "Mi 1era vez con NASM" ; const mensaje de un byte en memoria
	longitud EQU $-mensaje	;longitud q calcula el # caracteres de mensaje


section .text
    global _start
_start:
        ; *******IMPRIMIR EL MENSAJE *********
        mov eax, 4 ; tipo de subrutina, operacion => escritura, salida 
        mov ebx, 1  ; tipo de estandar, por teclado .Sin el int 80H es x el momento un mov de datos al registro ebx 
        mov ecx, mensaje ; el registro ecx se almacena la referencia al imprimir "mensaje"     .es x el momento un mov de datos al registro ecx
        mov edx, longitud ;  el registro edx se almacena la referencia a imprimir por # caracteres.       .es x el momento un mov de datos al registro edx
        int 80H ; es una interrupcion de Sw para el sistema operativo linux

        mov eax, 1 ;conla int 80h es salida del programa, llama a la subrutina system_exit, sys_exit
        mov ebx, 0  ; si el retorno es 0(200 en la web- ok)
        int 80H
