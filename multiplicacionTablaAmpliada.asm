;Realizado por Danny Vasquez
;Fecha 3 de agosto 2020

%macro imprimir 2
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro

section .data
    msj1 db '*'
    len_msj1 equ $ - msj1

    msj2 db '='
    len_msj2 equ $ - msj2

    nlinea		db		10,0
	lnlinea		equ		$ - nlinea

section .bss
    a resb 2
    b resb 2
    c resb 2

section .text
    global _start

_start:
    ;Definimos la primera vez el valor de a
    mov rax, 1
    add rax, '0' ;guarda como caracter
    mov [a] , rax
principal:
    
    push rax
    imprimir nlinea, lnlinea
    mov rcx, 1 ;Segundo factor o multiplicando

    mov rdx,[a]
    sub rdx, '0'
    cmp rdx,10
    jne ciclo

    jmp salir

ciclo:


    push rcx
    
    mov rax, [a] 
    sub rax, '0' ;transformando a digito   
    mul rcx
    add rax, '0' ;transformando a caracter

    
    add rcx, '0' ;Segundo multiplicando
    mov [b], rcx
    mov [c], rax ;Valor de la multiplicacion

       

    imprimir a, 1               ;Imprime 1er factor
    imprimir msj1, len_msj1     ;Imprime el signo
    imprimir b, 1               ;Imprime 2do factor
    imprimir msj2, len_msj2     ;Imprime el igual
    imprimir c, 1               ;Imprime el resultado
    imprimir nlinea, lnlinea


    pop rcx
    inc rcx
    cmp rcx, 10
    jne ciclo

    
    ;Para realizar las siguiente tablas
    pop rax
    inc rax 

    mov [a] , rax ;Para asginar otra vez el valor el primer factor
    jmp principal




salir:    
    mov eax, 1
    int 80h
