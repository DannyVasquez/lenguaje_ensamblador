;Realizado con el docente

%macro imprimir 2
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro


section .data
    
    msj db '*'



    nlinea		db		10,0
	lnlinea		equ		$ - nlinea

section .bss
    contador: resb 1

section .text
    global _start    

_start:
    
    mov bx, 9 ;filas
    mov cx, 9 ;columnas
    
   

principal: ;etiqueta  

    push bx
    cmp bx, 0
    jz salir
    jmp asterisco

asterisco:

    dec cx
    push cx
    imprimir msj, 1 ;el valor de ecx se reemplaza con msj='*'
    pop cx 
    cmp cx, 0
    jg asterisco ;jg, verifica q el primer operando sea mayor q el segundo operadno  del cmp
    imprimir nlinea, lnlinea

    pop bx
    dec bx
    mov cx,9
    jmp principal


salir:
    ;imprimir nlinea, lnlinea

    mov eax, 1
    int 80h