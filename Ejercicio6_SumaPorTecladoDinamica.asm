;Suma para números dinamicos ingresados por teclado
section .data

        mensaje_1 db "Ingrese 1er valor:",10
        len_mensaje_1 equ $-mensaje_1

        mensaje_2 db "Ingrese 2do valor:",10
        len_mensaje_2 equ $-mensaje_2


        resultado db "El resultado es:",10
        len_resultado equ $-resultado

        new_line db "",10
        len_new_line equ $-new_line

section .bss ;Declarar variables
        numero resb 1
        numero2 resb 1
        suma resb 1

section .text
         global _start
_start:

        ;*******************IMPRIME MENSAJE 1
        mov eax, 4
        mov ebx, 1
        mov ecx, mensaje_1
        mov edx, len_mensaje_1
        int 80H


        ;*******************LECTURA DE NUMERO
        mov eax, 3 ;
        mov ebx, 2
        mov ecx, numero 
        mov edx, 2
        int 80H

        ;*******************IMPRIME MENSAJE 1
        mov eax, 4
        mov ebx, 1
        mov ecx, mensaje_2
        mov edx, len_mensaje_2
        int 80H

        ;*******************LECTURA DE NUMERO 2
        mov eax, 3 ;
        mov ebx, 2
        mov ecx, numero2 
        mov edx, 2
        int 80H


        ;********************MOVIENDO VALORES
        mov ax, [numero] ;Para mover los datos desde memoria
        mov bx, [numero2]
        sub ax, '0' ; Valores convertidos en digito
        sub bx, '0' ; Valores convertidos en digito


        ;********************MOVIENDO VALORES
        add ax, bx
        add ax, '0' ; valores convertidos a cadena

        mov [suma], eax

        
        ;*******************PRESENTAR MENSAJE
        mov eax, 4 ;
        mov ebx, 1
        mov ecx, resultado 
        mov edx, len_resultado ; Se coloca siempre la longitud de caracteres
        int 80H



        ;*******************IMPRIMIR NUMERO
        mov eax, 4 ;
        mov ebx, 1
        mov ecx, suma 
        mov edx, 1 ; Se coloca siempre la longitud de caracteres
        int 80H

        ;*******************SALTO LINEA
        mov eax, 4
        mov ebx, 1
        mov ecx, new_line
        mov edx, len_new_line
        int 80H

        ;*******************   SALIDA DEL PROGRAMA
        mov eax, 1          ; salida cuando tenemos la interrupcion, salida del programa_exit, sys_exit
        int 80H
