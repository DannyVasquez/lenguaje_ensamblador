%macro imprimir 2
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro
section .data
	numero db "Serie fibonacci 1, "
	lenNumero equ $ -numero

	mensaj_coma db " , "
	lenComa equ $-mensaj_coma

	nlinea		db		10,0
	lnlinea		equ		$ - nlinea
	
section .bss

	numero_A resb 5
	numero_B resb 5

section .text
	global _start

_start:
	
	;Numeros iniciales del fibonacci
	mov eax,0
	mov ebx,1

	;Numeros totales q van a salir en la serie fibonacci
	mov ecx,8	
	
	;guardar los valores
	push rax
	push rbx
	push rcx

	;Imprime mensaje de Fibonacci y primeros valores
	imprimir numero, lenNumero

fibonacci:
	pop rcx
	pop rbx
	pop rax

	push rax
	push rbx
	push rcx

	mov [numero_A],ebx

	add eax,ebx
	add eax,'0'
	mov [numero_B],eax

	;Valor fibonacci
	imprimir numero_B, 5 

	;Imprimir una mensaj_coma
	imprimir mensaj_coma, lenComa

	pop rcx
	pop rbx
	pop rax
	
	mov eax,[numero_A]
	mov ebx,[numero_B]
	sub ebx,'0'


	push rax
	push rbx

	dec ecx
	push rcx

	loop fibonacci


salir:
	imprimir nlinea, lnlinea

	mov eax, 1
	mov ebx, 0
	int 80H