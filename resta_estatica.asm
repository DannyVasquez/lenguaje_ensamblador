;Danny Vinicio Vasquez Calderon
;Resta números estáticos

section .data

        resultado db "El resultado de la resta es:",10
        len_resultado equ $-resultado

        new_line db "",10
        len_new_line equ $-new_line

section .bss 
        resta resb 1

section .text
         global _start
_start:

        mov eax, 8 ; los toma valores como ASCII
        mov ebx, 5
        sub eax, ebx ; ;Realiza la resta
        add eax, '0' ; ajuste para transformarlos a cadena
        mov [resta],eax ;asigno el resultado 

        ;*******************IMPRIME MENSAJE PRESENTACION
        mov eax, 4
        mov ebx, 1
        mov ecx, resultado
        mov edx, len_resultado
        int 80H

        ;*******************IMPRIMIR NUMERO
        mov eax, 4 ;
        mov ebx, 1
        mov ecx, resta 
        mov edx, 5 ; Se coloca siempre la longitud de caracteres
        int 80H

        ;*******************SALTO LINEA
        mov eax, 4
        mov ebx, 1
        mov ecx, new_line
        mov edx, len_new_line
        int 80H

        ;*******************   SALIDA DEL PROGRAMA
        mov eax, 1          ; salida cuando tenemos la interrupcion, salida del programa_exit, sys_exit
        int 80H

        