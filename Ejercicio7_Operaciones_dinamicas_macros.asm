;Danny Vinicio Vasquez Calderon
;Realizado el Lunes,22 junio del 2020
;Operaciones basicas con macros

%macro leer 2
        ;*******************LECTURA DE NUMERO
        mov eax, 3 ;
        mov ebx, 2
        mov ecx, %1
        mov edx, %2
        int 80H
%endmacro


%macro imprimir 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80H

%endmacro

section .data
        
        mensaje_1 db "Ingrese 1er valor:",10
        len_mensaje_1 equ $-mensaje_1

        mensaje_2 db "Ingrese 2do valor:",10
        len_mensaje_2 equ $-mensaje_2


        resultado db "El resultado de la suma es:",10
        len_resultado equ $-resultado

        resultado2 db "El resultado de la resta es:",10
        len_resultado2 equ $-resultado2

        resultado3 db "El resultado de la multiplicacion es:",10
        len_resultado3 equ $-resultado3

        resultado4 db "El resultado de la division es:",10
        len_resultado4 equ $-resultado4

        resul_residuo db 10,"El residuo de la division es:",10
        len_resul_residuo equ $-resul_residuo 


        new_line db "",10
        len_new_line equ $-new_line

section .bss 

        numero resb 1
        numero2 resb 1

        suma resb 1
        resta resb 1
        multiplicacion resb 1
        division resb 1
        residuo resb 1

section .text
         global _start
_start:

        
        
        imprimir mensaje_1,  len_mensaje_1
        leer numero,  2

        imprimir mensaje_2,  len_mensaje_2
        leer numero2,  2
    
        ;********************MOVIENDO VALORES
        mov eax, [numero] ;Para mover los datos desde memoria
        mov ebx, [numero2]
        sub eax, '0' ; Valores convertidos en digito
        sub ebx, '0' ; Valores convertidos en digito


        ;*******************SUMA
        add eax, ebx ; ;Realiza la suma
        add eax, '0' ; ajuste para transformarlos a cadena
        mov [suma],eax ;asigno el resultado 

        
        imprimir resultado,  len_resultado
        imprimir suma,1

        ;*******************SALTO LINEA
        mov eax, 4
        mov ebx, 1
        mov ecx, new_line
        mov edx, len_new_line
        int 80H

    

        ;********************MOVIENDO VALORES
        mov eax, [numero] ;Para mover los datos desde memoria
        mov ebx, [numero2]
        sub eax, '0' ; Valores convertidos en digito
        sub ebx, '0' ; Valores convertidos en digito


        ;*******************Resta
        sub eax, ebx ; ;Realiza la resta
        add eax, '0' ; ajuste para transformarlos a cadena
        mov [resta],eax ;asigno el resultado 

        imprimir resultado2,  len_resultado2
        imprimir resta,1


        ;*******************SALTO LINEA
        mov eax, 4
        mov ebx, 1
        mov ecx, new_line
        mov edx, len_new_line
        int 80H


        ;********************MOVIENDO VALORES
        mov eax, [numero] ;Para mover los datos desde memoria
        mov ebx, [numero2]
        sub eax, '0' ; Valores convertidos en digito
        sub ebx, '0' ; Valores convertidos en digito

        ;*******************Multiplicacion
  
        mul ebx ;Realiza la multiplicacion mul eax= eax * ebx 
        add eax, '0' ; ajuste para transformarlos a cadena
        mov [multiplicacion],eax ;asigno el resultado 

        imprimir resultado3,  len_resultado3
        imprimir multiplicacion,1

        ;*******************SALTO LINEA
        mov eax, 4
        mov ebx, 1
        mov ecx, new_line
        mov edx, len_new_line
        int 80H


        ;********************MOVIENDO VALORES
        mov al, [numero] ;Para mover los datos desde memoria
        mov bh, [numero2]
        sub al, '0' ; Valores convertidos en digito
        sub bh, '0' ; Valores convertidos en digito


        ;*******************DIVISION

        div bh         ;Cuando existe un overflow, o acarreo se ocupa ah y se ocupa registro "DX"
        add al, '0'  
        mov [division],al ;Cociente
        add ah, '0'
        mov [residuo], ah
    
        
        imprimir resultado4,  len_resultado4
        imprimir division,1

        imprimir resul_residuo,  len_resul_residuo
        imprimir residuo,1


        ;*******************SALTO LINEA
        mov eax, 4
        mov ebx, 1
        mov ecx, new_line
        mov edx, len_new_line
        int 80H

        ;*******************   SALIDA DEL PROGRAMA
        mov eax, 1          ; salida cuando tenemos la interrupcion, salida del programa_exit, sys_exit
        int 80H
