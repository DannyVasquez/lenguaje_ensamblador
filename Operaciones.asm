;Danny Vinicio Vasquez Calderon
;Operaciones basicas

%macro imprimir 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80H

%endmacro

section .data

        resultado db "El resultado de la suma es:",10
        len_resultado equ $-resultado

        resultado2 db "El resultado de la resta es:",10
        len_resultado2 equ $-resultado2

        resultado3 db "El resultado de la multiplicacion es:",10
        len_resultado3 equ $-resultado3

        resultado4 db "El resultado de la division es:",10
        len_resultado4 equ $-resultado4

        resul_residuo db 10,"El residuo de la division es:",10
        len_resul_residuo equ $-resul_residuo 


        new_line db "",10
        len_new_line equ $-new_line

section .bss 
        suma resb 1
        resta resb 1
        multiplicacion resb 1
        division resb 1
        residuo resb 1

section .text
         global _start
_start:

        ;*******************SUMA
        mov eax, 4 ; los toma valores como ASCII
        mov ebx, 2
        add eax, ebx ; ;Realiza la suma
        add eax, '0' ; ajuste para transformarlos a cadena
        mov [suma],eax ;asigno el resultado 

        
        
        ;*******************IMPRIME MENSAJE PRESENTACION 1
        ;mov eax, 4
        ;mov ebx, 1
        ;mov ecx, resultado
        ;mov edx, len_resultado
        ;int 80H

        imprimir resultado,  len_resultado
        imprimir suma,1
        ;*******************IMPRIMIR NUMERO SUMA
        ;mov eax, 4 ;
        ;mov ebx, 1
        ;mov ecx, suma 
        ;mov edx, 5 ; Se coloca siempre la longitud de caracteres
        ;int 80H

        ;*******************SALTO LINEA
        mov eax, 4
        mov ebx, 1
        mov ecx, new_line
        mov edx, len_new_line
        int 80H




        ;*******************Resta

        mov eax, 4 ; los toma valores como ASCII
        mov ebx, 2
        sub eax, ebx ; ;Realiza la resta
        add eax, '0' ; ajuste para transformarlos a cadena
        mov [resta],eax ;asigno el resultado 

        ;*******************IMPRIME MENSAJE PRESENTACION 2
        ;mov eax, 4
        ;mov ebx, 1
        ;mov ecx, resultado2
        ;mov edx, len_resultado2
        ;int 80H

        imprimir resultado2,  len_resultado2
        imprimir resta,1
        ;*******************IMPRIMIR NUMERO Resta
        ;mov eax, 4 ;
        ;mov ebx, 1
        ;mov ecx, resta 
        ;mov edx, 5 ; Se coloca siempre la longitud de caracteres
        ;int 80H

        ;*******************SALTO LINEA
        mov eax, 4
        mov ebx, 1
        mov ecx, new_line
        mov edx, len_new_line
        int 80H








        ;*******************Multiplicacion
        mov eax, 4; los toma valores como ASCII
        mov ebx, 2
        mul ebx ;Realiza la multiplicacion mul eax= eax * ebx 
        add eax, '0' ; ajuste para transformarlos a cadena
        mov [multiplicacion],eax ;asigno el resultado 


        ;*******************IMPRIME MENSAJE PRESENTACION 3
        ;mov eax, 4
        ;mov ebx, 1
        ;mov ecx, resultado3
        ;mov edx, len_resultado3
        ;int 80H
        

        imprimir resultado3,  len_resultado3
        imprimir multiplicacion,1

        ;*******************IMPRIMIR NUMERO
        ;mov eax, 4 ;
        ;mov ebx, 1
        ;mov ecx, multiplicacion 
        ;mov edx, 5 ; Se coloca siempre la longitud de caracteres
        ;int 80H

        ;*******************SALTO LINEA
        mov eax, 4
        mov ebx, 1
        mov ecx, new_line
        mov edx, len_new_line
        int 80H



        ;*******************DIVISION

        mov al, 5
        mov bh, 2
        div bh         ;Cuando existe un overflow, o acarreo se ocupa ah y se ocupa registro "DX"
        add al, '0'  
        mov [division],al ;Cociente
        add ah, '0'
        mov [residuo], ah

        

        ;*******************IMPRIME MENSAJE PRESENTACION 4(RESULTADO DIVISION)
        ;mov eax, 4
        ;mov ebx, 1
        ;mov ecx, resultado4
        ;mov edx, len_resultado4
        ;int 80H
        
        imprimir resultado4,  len_resultado4
        imprimir division,1

        ;*******************IMPRIMIR NUMERO
        ;mov eax, 4 ;
        ;mov ebx, 1
        ;mov ecx, division 
        ;mov edx, 1 ; Se coloca siempre la longitud de caracteres
        ;int 80H

        ;*******************IMPRIME MENSAJE PRESENTACION 4(RESULTADO RESIDUO)
        ;mov eax, 4
        ;mov ebx, 1
        ;mov ecx, resul_residuo
        ;mov edx, len_resul_residuo
        ;int 80H

        imprimir resul_residuo,  len_resul_residuo
        imprimir residuo,1
        ;*******************IMPRIMIR NUMERO
        ;mov eax, 4 ;
        ;mov ebx, 1
        ;mov ecx, residuo 
        ;mov edx, 1 ; Se coloca siempre la longitud de caracteres
        ;int 80H
        

        


        ;*******************SALTO LINEA
        mov eax, 4
        mov ebx, 1
        mov ecx, new_line
        mov edx, len_new_line
        int 80H

        ;*******************   SALIDA DEL PROGRAMA
        mov eax, 1          ; salida cuando tenemos la interrupcion, salida del programa_exit, sys_exit
        int 80H

        