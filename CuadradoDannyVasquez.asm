;Danny Vasquez
;Realizado el 22 de julio 2020
;Sale error

%macro imprimir 2
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro



section .data

    nlinea		db		10,10,0
	lnlinea		equ		$ - nlinea

    asterisco db '*'

    
section .bss
  	contador:		resb 5

section .text
    global _start    

_start:
    
    mov ecx, 9 ;horizontal
    mov edx,9
    mov [contador], 0

imprimirBloque: ;etiqueta  

    dec ecx 
    push rcx  
    imprimir asterisco, 1
    pop rcx    
    cmp ecx,0

    jnz imprimirBloque
 
    jmp saltoLinea

    mov ecx, 9
    inc contador
    cmp contador,8
    jnz imprimirBloque

    jmp salir



saltoLinea:
    mov eax, 4
	mov ebx, 1
	mov ecx, nlinea
	mov edx, lnlinea
	int 80h      


salir:
    mov eax, 1
    int 80h   