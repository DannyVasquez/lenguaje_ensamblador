%macro imprimir 2
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro


section .data
    msj1 db "Ingrese 5 numeros",10
    len1 equ $-msj1

    msj2 db "Numero mayor es:",10
    len2 equ $-msj2

    arreglo db 0,0,0,0,0
    len_arreglo equ $-arreglo

section .bss
    numero resb 2 

section .data 
    global _start  

_start:

    mov esi, arreglo ;esi permite fijar el tamaño del arreglo, posicionar el arreglo en memoria
    mov edi,  0         ;edi va a contener el indice del arreglo  

    imprimir msj1, len1 ;mensaje uno

leer:
    mov eax, 3
    mov ebx, 0
    mov ecx, numero
    mov edx, 2 
    int 80h

    mov al, [numero]
    sub al, 0

    mov[esi], al ; mover el valor a un indice del arreglo

    inc edi 
    inc esi ;indice del arreglo

    cmp edi, len_arreglo
    jb leer

    mov ecx, 0
    mov bl, 0

comparacion:    
    mov al,[arreglo + ecx]   ;Para acceder a una posicion especifica
    cmp al, bl 
    jb contador ; salta cuando el 1ero es menor q el 2do ; Si es menor salta
    mov bl, al
contador:
    inc ecx 
    cmp ecx, len_arreglo
    jb comparacion

imprimirValor:
    add bl, 0
    mov [numero], bl    

    imprimir msj2, len2 ;mensaje dos

    imprimir numero, 1

salir: 
    mov eax, 1 
    int 80h


