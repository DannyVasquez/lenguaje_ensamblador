;Programa que imprime

%macro imprimir 2
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro


section .data
    asterisco db '*'

    nlinea		db		10,10,0
	lnlinea		equ		$ - nlinea

section .text
    global _start    

_start:
    
    mov cx, 9

imprimirBloque: ;etiqueta  

    dec cx ;el decrementamos el valor q esta en la pila y se vuelve un menor menor 
    push cx  
    imprimir asterisco, 1
    pop cx
    
    cmp cx,0
    jnz imprimirBloque
    ;jmp imprimirBloque
    

saltoLinea:
    mov eax, 4
	mov ebx, 1
	mov ecx, nlinea
	mov edx, lnlinea
	int 80h      

salir:
    mov eax, 1
    int 80h