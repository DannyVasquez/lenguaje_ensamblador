;Realizado por Vasquez Calderon Danny Vinicio
;Fecha: 24 de Agosto del 2020.          6to "A"
;TEMA 4: Generar el factorial de un número. 

%macro imprimir 2
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro

%macro leer 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro


section	.data
	mensa_Presentacion db "Ingrese el valor para calcular el factorial: "
	len_mens_Presentacion equ $ - mensa_Presentacion	

	mensa_Fact db "Factorial es: "	
	len_mensa_Fact equ $ - mensa_Fact	
	
	nueva_lineaa		db		10,0
	len_nueva_linea		equ		$ - nueva_lineaa	

section .bss
	valor resb 5
	factorial resb 5
	
section	.text
   global _start        
	
_start:                 
   imprimir  mensa_Presentacion,len_mens_Presentacion	 
   leer valor, 5                    ;Factorial de un numero ingresado

   mov al, [valor]                  ;Valor ingresaso por pantalla
   sub al, '0'
   mov bl, 1                        
   mov cl, al

   cmp al, 0
   je factorialCero                 ;Caso excepciòn, si es el factorial de 0
   jne facto

facto:
   
   mul bl                           ;ax = ax * bx => 3 x 1 =3;  	
   mov [factorial], al					
   dec cl                           ;al = cl 
   mov al, cl
   mov bl, [factorial]              ;Guardar el valor valor resultante en la bl
   cmp cl, 0
   jg facto                         ;Si el cl > 0, salta

   jmp resultado

factorialCero:

   mov al, 1
   add al, '0'
   mov [factorial], al                  ;Resultado del factorial

   imprimir mensa_Fact,len_mensa_Fact
   imprimir factorial,5
   imprimir nueva_lineaa, len_nueva_linea

   jmp salir

   
resultado:

   add bl, '0'
   mov [factorial], bl                  ;Resultado del factorial

   imprimir mensa_Fact,len_mensa_Fact
   imprimir factorial,5
   imprimir nueva_lineaa, len_nueva_linea

   jmp salir   


   
salir:
   mov	  eax,1        
   int	  80h
