%macro leer 2 
        mov eax, 3
        mov ebx, 0
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

%macro imprimir 2 
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro


section .data

        nueva_linea db 10,'',10
        len_nueva equ $-nueva_linea

        men1 db  "Dividendo:",10
        leen1 equ $ - men1

        men2 db   "Divisor :",10
        leen2 equ $ - men2

        men3 db 10,"Cociente: ",10
        leen3 equ $ - men3

        men4 db 10,"Residuo: ",10
        leen4 equ $ - men4

section .bss

        cociente resb 2
        residuo resb 2
        num1 resb 2
        num2 resb 2
       


section .text

        global _start 

_start:
    
    imprimir men1, leen1 
    leer num1, 2    
    mov ax, [num1]
    sub ax, '0'
    


    imprimir men2, leen2
    leer num2, 2 
	mov bx, [num2]
    sub bx, '0'

	mov cx, 0       ; contador
	

ciclo: 


    sub ax, bx  ; al=al-bl     6-3=3

	inc cx    ; el numero de veces que reste para bl (cociente)


	cmp ax,bx
	jnb ciclo   ; salta cuando el primer operando es mayor


res: 

	add al,'0'  
    
	add cx,'0'  


    
	mov [cociente], cx
    
    
	mov [residuo], al


    ; precento el cociente
    imprimir men3, leen3
    imprimir  cociente,2

    ;presento el residuo
    imprimir nueva_linea,len_nueva
    imprimir men4, leen4

    imprimir residuo,2
    imprimir nueva_linea,len_nueva

       
    mov eax, 1
    int 80h