;Realizado por Danny Vasquez
;Fecha 29/07/2020

%macro imprimir 2
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro


section .data
    
    msj db '*'

    nlinea		db		10,0
	lnlinea		equ		$ - nlinea

section .text
    global _start    

_start:
    
    mov bx, 9 ;filas
    mov cx, 9 ;columnas
    
   
principal: 

    push rbx
    cmp ebx, 0
    jz salir
    jmp asterisco

asterisco:



    push rcx
    imprimir msj, 1 
    pop rcx 
    loop asterisco

    imprimir nlinea, lnlinea
    pop rbx 
    dec ebx
    mov ecx, ebx ; Se restablece el valor a 9 si se desea formar un cuadrado
    jmp principal


salir:
    mov eax, 1
    int 80h