%macro escribir 2
	mov eax,4  
	mov ebx,1
	mov ecx,%1
	mov edx,%2
	int 80h
%endmacro

%macro leer 2
	mov eax,3  
	mov ebx,2
	mov ecx,%1
	mov edx,%2
	int 80h
%endmacro

section .data
    asterisco db ' '
    vacio db '*'
    new_line db 10,''
section .bss
 

section .text
    global _start

_start:

    mov ecx, 9
  

l1:
    push rcx
    call imprimir
    pop rcx
    push rcx

    mov eax, 9
    mov ebx, ecx
    sub eax, ebx 

    cmp ecx, 9
    je vacios
    jmp vacios

l2:
    push rcx
    call imprimir_asterisco
    pop rcx

    loop l2

    pop rcx
    loop l1

    jmp salir

vacios:
    push rax
    push rcx
    escribir vacio, 1
    pop rcx
    pop rax
    dec eax
    cmp eax, 0
    jg vacios
    je vacios
    jmp l2


imprimir:
    mov eax, 4
    mov ebx, 1
    mov ecx, new_line
    mov edx, 1
    int 80h

    ret 

imprimir_asterisco:
    mov eax, 4
    mov ebx, 1
    mov ecx, asterisco
    mov edx, 1
    int 80h

    ret
            

salir:
    mov eax, 4
    mov ebx, 1
    mov ecx, new_line
    mov edx, 1
    int 80h

    mov eax, 1
    int 80h