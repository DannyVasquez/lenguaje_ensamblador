%macro leer 2 
        mov eax, 3
        mov ebx, 0
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

%macro imprimir 2 
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro


section .data


        men5 db "Debe ingresar un dividendo que sea mayor al divisor",10
        leen5 equ $ - men5
        
        nueva_linea db '',10
        len_nueva equ $-nueva_linea

        men1 db  "Dividendo:",10
        leen1 equ $ - men1

        men2 db   "Divisor :",10
        leen2 equ $ - men2

        men3 db  "Cociente: ",10
        leen3 equ $ - men3

        men4 db "Residuo: ",10
        leen4 equ $ - men4

        

section .bss

        cociente resb 2
        residuo resb 2
        num1 resb 2
        num2 resb 2
       

section .text

        global _start 


_start:
    

    imprimir nueva_linea,len_nueva
    imprimir men1, leen1 
    leer num1, 2    
    
    
    imprimir men2, leen2
    leer num2, 2 

    mov al, [num1]
    sub al, '0'
	mov bl, [num2]
    sub bl, '0'
	

    
    cmp al,bl
    jl error  ; en caso el dividendo ingresado sea menor al divisor
     
    mov ecx, 0       ; contador
    jmp ciclo
    

ciclo: 

    sub al, bl  ; 
	inc ecx    ;  (cociente) ( al = residuo)


	cmp al,bl
	jg ciclo   
    cmp al,bl  
    je ciclo   


	jmp resul



error:
 
    imprimir nueva_linea,len_nueva
    imprimir men5,leen5
    imprimir nueva_linea,len_nueva

    jmp salir 




resul: 


    add ecx,'0' 
	add al,'0'  
	mov [cociente], ecx
	mov [residuo], al

    ; precento el cociente
    imprimir nueva_linea,len_nueva
    imprimir nueva_linea,len_nueva
    imprimir men3, leen3
    imprimir  cociente,2

    ;presento el residuo
    imprimir nueva_linea,len_nueva
    imprimir men4, leen4
    imprimir residuo,2
    imprimir nueva_linea,len_nueva

    jmp salir




salir:

    imprimir nueva_linea,len_nueva
    mov eax, 1
    int 80h