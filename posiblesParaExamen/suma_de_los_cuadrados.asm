%macro imprimir 2
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro

%macro leer 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro


section	.data
	mensa_Presentacion db "Ingrese un numero: "
	len_mens_Presentacion equ $ - mensa_Presentacion	

    mensa_Presentacion2 db "Ingrese un numero mayor a 0"
	len_mens_Presentacion2 equ $ - mensa_Presentacion2	

	mensa_Resultado db "Resultado: "	
	len_mensa_Resultado equ $ - mensa_Resultado	

    mensa_signo db " + "	
	len_mensa_signo equ $ - mensa_signo
	
	nueva_linea	db		10,0
	len_nueva_linea		equ		$ - nueva_linea	

section .bss
	contador resb 5
	cuadrado resb 5
	
section	.text
   global _start        
	
_start:         

   imprimir  mensa_Presentacion,len_mens_Presentacion	 
   leer contador, 5     

   mov dl, [contador]                  ;contador ingresaso por pantalla
   sub dl, '0'
   push rdx 

   cmp dl, 0
   je cero                 ;Caso excepciòn
   jne mensaje

mensaje:
    imprimir mensa_Resultado, len_mensa_Resultado
    mov al, 1   ;Primer cuadrado  

    pop rdx
    mov [contador], rdx      ;Opera como digito

    push rax

    jmp Suma_Cuadrados
Suma_Cuadrados:

    

    mul al
    add al, '0'
    mov [cuadrado],al

    imprimir cuadrado, 5
    imprimir mensa_signo, len_mensa_signo

    mov dl, [contador]                  ;contador ingresaso por pantalla
    dec dl
    mov cl, dl  ;Asigno valor para comparar
    mov [contador], dl

    pop rax
    inc rax
    push rax

    cmp cl,0
    jg Suma_Cuadrados
    
    jmp salir



cero:

    imprimir  mensa_Presentacion2,len_mens_Presentacion2	
    imprimir  nueva_linea, len_nueva_linea
   
salir:
   imprimir  nueva_linea, len_nueva_linea
   mov	  eax,1        
   int	  80h
