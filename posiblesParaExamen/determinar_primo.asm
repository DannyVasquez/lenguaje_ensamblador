%macro imprimir 2
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro

%macro leer 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro


section	.data
	mensa_Presentacion db "Ingrese un numero: "
	len_mens_Presentacion equ $ - mensa_Presentacion	

    mensa_Presentacion2 db "El numero "
	len_mens_Presentacion2 equ $ - mensa_Presentacion2	

	mensa_Primo db "es PRIMO "	
	len_mensa_Primo equ $ - mensa_Primo	

    mensa_No_Primo db "NO es PRIMO"	
	len_mensa_No_Primo equ $ - mensa_No_Primo
	
	nueva_linea	db		10,0
	len_nueva_linea		equ		$ - nueva_linea	

section .bss
	numero resb 2
	contador resb 2    ;Si solo se incrementa en 2 veces es primo
    numero_B resb 2    ;DECREMENTA
	division resb 2
section	.text
   global _start        
	
_start:         

    mov cx, 0
    mov [contador], cx

    imprimir mensa_Presentacion, len_mens_Presentacion
    leer numero, 2

    mov bh,[numero]
    sub bh, '0'
    mov [numero], bh
    mov [numero_B], bh

    cmp bh,0
    
Ciclo:

    mov ax,0        ;CUANDO SE TRABAHA CON REGISTROS DE 8bits sale error de CORE
    mov bx, 0       ;ESTAS 2 lineas corrige el error

    mov al, [numero]
    mov bh, [numero_B]
    div bh         
    
    cmp ah, 0       ;RESIDUO
    je Comparacion1 

    jmp Comparacion2
    
  
Comparacion1: ;Si el residuo es CERO, SI incrementa contador
    
    dec bh
    mov [numero_B], bh

    ;Incrementa 
    mov cl,[contador]
    inc cl
    mov [contador], cl


    cmp bh,0
    jg Ciclo
    jmp imprimirResultado
Comparacion2: ;Si el residuo es DIFERENTE a UNO, NO incrementa contador
    
    dec bh
    mov [numero_B], bh
    
    cmp bh,0
    jg Ciclo
    jmp imprimirResultado

;imprimirResultado:
;    mov cx,[contador]
;    add cx, '0'
;    mov [contador], cx
;    imprimir contador,2
;    jmp salir    

imprimirResultado:
    mov cl,[contador]
    cmp cl,2
    je primo
    jmp no_primo

primo:
    imprimir mensa_Primo,len_mensa_Primo 
    jmp salir
no_primo:
    imprimir mensa_No_Primo,len_mensa_No_Primo 
    jmp salir

salir:
    imprimir nueva_linea,len_nueva_linea

    mov eax,1
    int 80h    