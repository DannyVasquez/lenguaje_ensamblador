
;Danny Michael Jaramillo Jumbo
; miercoles 26 de agosto 

; TRABAJE CON una cadena de 9 caracteres.
%macro presentar 2
	mov eax,4
	mov ebx,1
	mov ecx,%1 
	mov edx,%2
	int 80H	
%endmacro

%macro leer 2
    mov eax,3
    mov ebx,0
    mov ecx,%1 
    mov edx,%2
    int 80H 
%endmacro

section .bss


	arrelorecogido resb 30

	idarchivo resd 1
	idarchivo2 resd 1
	
	
	contadorNum resb 10
	indice resb 10
	
    res resb 2
    contador_a resb 2
    contador_b resb 2


	mayorNUmero resb 2
    menorNUmero resb 2
    


section .data

	msg2 db 'La cadena invertida es :',10
	lenMsj2 equ $-msg2

	msj1 db "La cadena ingresada es",10
	lenMsg1 equ $- msj1


    arreglo db 0,0,0,0,0,0,0,0,0
	lenArreglo equ $- arreglo

    nueva_linea db '',10
    len_nueva equ $-nueva_linea	

	archivo_entrada db "/home/dannyvasquez/Escritorio/Ejercicios_ensamblador/ExamenArchivos/datos.txt",0 
	archivo_salida db "/home/dannyvasquez/Escritorio/Ejercicios_ensamblador/ExamenArchivos/resultados.txt",0

    

    textoGuardar db "000000000"
	lenTextoGuardar equ $-textoGuardar


section .text
    global _start    

_start:
	
;***** ARCHIVO 1

	mov eax,5 		
	mov ebx,archivo_entrada 	
	mov ecx,0 	
	mov edx,0 		
	int 80H 	

	test eax,eax 
	jz salir

	
	mov dword [idarchivo],eax ; 
	
	mov eax,3
	mov ebx,[idarchivo]
	mov ecx,arrelorecogido
	mov edx,30
	int 80H

    presentar nueva_linea,len_nueva
    presentar msj1,lenMsg1
    presentar nueva_linea,len_nueva
    presentar arrelorecogido,10
    presentar nueva_linea,len_nueva


	
	
	mov eax,6		 		
	mov ebx,[idarchivo] 	
	mov ecx,0 				
	mov edx,0 				
	int 80H 				



; *****************************************************************************************

	mov esi ,arreglo 
	mov edi,0 

	mov ecx,0


LeerArreglodelArchivp:

	mov al,[arrelorecogido+ecx]	
	sub al,'0'           
   
	mov[esi],al
	inc esi
	inc edi
	inc ecx

	cmp edi,lenArreglo 
	jl LeerArreglodelArchivp	
	
  

finalpresentar:

presentar nueva_linea,len_nueva
presentar msg2,lenMsj2

presentar nueva_linea,len_nueva


mov ecx,0

mov eax,lenArreglo
dec eax

mov ebx,eax ; para posicionar al reves los valores

invertir:
	
	mov al,[arreglo+ecx]
	add al,'0'
	
    mov [textoGuardar+ebx],al

    dec ebx
	inc ecx

	cmp ecx,lenArreglo

	jl invertir   
	jmp salir


salir:

    presentar textoGuardar,lenTextoGuardar
	
	mov eax,8		
	mov ebx,archivo_salida 	
	mov ecx,1	 		
	mov edx,0 		
	int 80H 		

	test eax,eax 
	jz salir

	
	mov dword [idarchivo2],eax 
	
	mov eax,4
	mov ebx,[idarchivo2]
	mov ecx,textoGuardar
	mov edx,lenTextoGuardar
	int 80H



	mov eax,6		 		
	mov ebx,[idarchivo2] 	
	mov ecx,0 				
	mov edx,0 				
	int 80H 
    
    presentar nueva_linea,len_nueva
    presentar nueva_linea,len_nueva
	mov eax, 1
	mov ebx, 0
	int 80H
         




    
