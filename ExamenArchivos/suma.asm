%macro imprimir 2
	mov eax,4
	mov ebx,1
	mov ecx,%1 
	mov edx,%2
	int 80H	
%endmacro

%macro leer 2
    mov eax,3
    mov ebx,0
    mov ecx,%1 
    mov edx,%2
    int 80H 
%endmacro

; ecx,modo de acceso
; edx, permisos
section .bss
	NUM1 resb 30
	NUM2 resb 30
	idarchivo resd 1
	idarchivo2 resd 1
	idarchivo3 resd 1
	
	auxa resb 10
	

	contadorNum resb 10
	indice resb 10
	


section .data
	msj1 db "LEER EL ARCHIVO",10
	lenMsg1 equ $- msj1





	path1 db "/home/dannyvasquez/Escritorio/Ejercicios_ensamblador/ExamenArchivos/dato_1.txt",0 ; cero para cerrar el archivo
	path2 db "/home/dannyvasquez/Escritorio/Ejercicios_ensamblador/ExamenArchivos/dato_2.txt",0 ; cero para cerrar el archivo
    path3 db "/home/dannyvasquez/Escritorio/Ejercicios_ensamblador/ExamenArchivos/dato_3.txt",0 ; cero para cerrar el archivo


section .NUM1
    global _start    

_start:
	



;* apertura del archivo ***
	mov eax,5 		;servicio para abrir el archivo/ tambien id del archivo
	mov ebx,path1 	;direccion del archivo
	mov ecx,0 		;modo de acceso solo lectura
	mov edx,0 		;permisos del archivo
	int 80H 		;igual a int 80H

	test eax,eax ; es igual que el and pero solo modifica las banderas no guarda en eax
	jz salir

	;* archivo sin exepciones **
	mov dword [idarchivo],eax ; respaldamos el id del archivo
	;imprimir msj1 ,lenMsg1
	
	mov eax,3
	mov ebx,[idarchivo]
	mov ecx,NUM1
	mov edx,30
	int 80H


	
	;* cerrar el archivo ***
	mov eax,6		 		;servicio para abrir el archivo
	mov ebx,[idarchivo] 	;direccion del archivo
	mov ecx,0 				;modo de acceso solo lectura
	mov edx,0 				;permisos del archivo
	int 80H 				;igual a int 80H


; *******************************



;* apertura del archivo ***
	mov eax,5 		;servicio para abrir el archivo/ tambien id del archivo
	mov ebx,path2 	;direccion del archivo
	mov ecx,0 		;modo de acceso solo lectura
	mov edx,0 		;permisos del archivo
	int 80H 		;igual a int 80H

	test eax,eax ; es igual que el and pero solo modifica las banderas no guarda en eax
	jz salir

	;* archivo sin exepciones **
	mov dword [idarchivo2],eax ; respaldamos el id del archivo
	;imprimir msj1 ,lenMsg1
	
	mov eax,3
	mov ebx,[idarchivo2]
	mov ecx,NUM2
	mov edx,30
	int 80H
	
	;imprimir NUM2,30
	;* cerrar el archivo ***
	mov eax,6		 		;servicio para abrir el archivo
	mov ebx,[idarchivo2] 	;direccion del archivo
	mov ecx,0 				;modo de acceso solo lectura
	mov edx,0 				;permisos del archivo
	int 80H 				;igual a int 80H

;**** llenamos el arreglo

  
   mov ax,[NUM1]
   sub ax,'0'

   mov bx,[NUM2]
   SUB bx,'0'

   add ax,bx

   add ax,'0'

   mov [auxa],ax

   

guardarEnArchivo:

	;* apertura del archivo ***
	mov eax,5 		;servicio para abrir el archivo/ tambien id del archivo
	mov ebx,path3 	;direccion del archivo
	mov ecx,1	 		;modo de acceso solo lectura
	mov edx,0 		;permisos del archivo
	int 80H 		;igual a int 80H

	test eax,eax ; es igual que el and pero solo modifica las banderas no guarda en eax
	jz salir

	;* archivo sin exepciones **
	mov dword [idarchivo3],eax ; respaldamos el id del archivo
	;imprimir msj1 ,lenMsg1
	
	mov eax,4
	mov ebx,[idarchivo3]
	mov ecx,auxa 
	mov edx,1
	int 80H

	;* cerrar el archivo ***
	mov eax,6		 		;servicio para abrir el archivo
	mov ebx,[idarchivo3] 	;direccion del archivo
	mov ecx,0 				;modo de acceso solo lectura
	mov edx,0 				;permisos del archivo
	int 80H 				;igual a int 80H


salir:
	
	imprimir auxa,2
	
	mov eax, 1
	int 80H