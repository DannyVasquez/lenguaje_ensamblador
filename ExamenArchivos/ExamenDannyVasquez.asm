;Realizado por Vasquez Calderon Danny Vinicio
;Fecha: 26 de Agosto del 2020.          6to "A"

;TEMA 4: Construir un programa en ensamblador que permite:
;•Leer los datos del datos.txt.
;•Determinar que vocal es la que menos se repite en el archivo
;•Guardar el resultado en el archivo resultados.txt

%macro imprimir 2
	mov eax,4
	mov ebx,1
	mov ecx,%1 
	mov edx,%2
	int 80H	
%endmacro

%macro leer 2
    mov eax,3
    mov ebx,0
    mov ecx,%1 
    mov edx,%2
    int 80H 
%endmacro

section .bss
	cadena resb 30

	id_archivo resd 1
	id_archivo2 resd 1

    resultado resb 10

	vocal_A	resd 5
	vocal_E resd 5
	vocal_I resd 5
	vocal_O resd 5
	vocal_U resd 5

	

section .data
	msj1 db "Primer Dato: "
	lenMsg1 equ $- msj1

	vocales db "La vocal menor repetida es: "
	len_vocales equ $- vocales

	msg db 10,'a '
	len equ $- msg

	msg_2 db 10,'e ',10
	len_2 equ $- msg_2

	msg_3 db 10,'i ',10
	len_3 equ $- msg_3

	msg_4 db 10,'O ',10
	len_4 equ $- msg_4

	msg_5 db 10,'u ',10
	len_5 equ $- msg_5



	textoGuardar db "0"
	lenTextoGuardar equ $-textoGuardar


	salto db '',10
	lenSalto equ $- salto

	arreglo db 0,0,0,0,0
	lenArreglo equ $- arreglo

	arreglo_2 db 0,0,0,0,0
	lenArreglo_2 equ $- arreglo_2

mensaje_3 db 10,'Los valores ya ordenados son:',10
	lenMsj2 equ $-mensaje_3


	path1 db "/home/dannyvasquez/Escritorio/Ejercicios_ensamblador/ExamenArchivos/datos.txt",0 ; cero para cerrar el archivo
	path2 db "/home/dannyvasquez/Escritorio/Ejercicios_ensamblador/ExamenArchivos/resultados.txt",0 ; cero para cerrar el archivo
    
section .text
    global _start    

_start:
	;********* apertura del archivo *******************
	mov eax,5 		;servicio para abrir el archivo/ tambien id del archivo
	mov ebx,path1 	;direccion del archivo
	mov ecx,0 		;modo de acceso solo lectura
	mov edx,0 		;permisos del archivo
	int 80H 		;igual a int 80H

	test eax,eax ; es igual que el and pero solo modifica las banderas no guarda en eax
	jz salir

	;********* archivo sin exepciones ****************
	mov dword [id_archivo],eax ; respaldamos el id del archivo
	;imprimir msj1 ,lenMsg1
	
	mov eax,3
	mov ebx,[id_archivo]
	mov ecx,cadena
	mov edx,30
	int 80H

	;imprimir cadena,30
	;imprimir salto,lenSalto


	;********* cerrar el archivo *****************
	mov eax,6		 		;servicio para abrir el archivo
	mov ebx,[id_archivo] 	;direccion del archivo
	mov ecx,0 				;modo de acceso solo lectura
	mov edx,0 				;permisos del archivo
	int 80H 				;igual a int 80H


;*********************************************
;*************************************************OPERACION
    mov ax,0
    mov [vocal_A],ax
	mov [vocal_E],ax
	mov [vocal_I],ax
	mov [vocal_O],ax
	mov [vocal_U],ax

	mov esi ,cadena ;Indice inicial para recorrer el arreglo
	mov edi,0 ; el limite para recorrer.

	mov ecx,0


leerArreglo:
 
    ;para mover todos los valores leidos desde el archivo al arreglo
	mov al,[cadena+ecx]	
	sub al,'0'           ; le resto cero para guardarlos operables
	mov[esi],al
	inc esi
	inc edi
	inc ecx


	;COMPROBACION
	cmp al, 49
	je letra_A

	cmp ax, 53
	je letra_E

	cmp ax, 57
	je letra_I

	cmp ax, 63
	je letra_O

	cmp ax, 69
	je letra_U

	cmp edi,lenArreglo ;cuando edi es menor
	jb leerArreglo

	jmp imprimirResultado	

		
	mov al,0

letra_A:

	mov bx,[vocal_A]
	inc bx 
	mov [vocal_A], bx

	cmp edi,lenArreglo ;cuando edi es menor
	jb leerArreglo

	jmp imprimirResultado	



letra_E:
	mov bx,[vocal_E]
	inc bx 
	mov [vocal_E], bx

	cmp edi,lenArreglo ;cuando edi es menor
	jb leerArreglo

	jmp imprimirResultado	

letra_I:
	mov bx,[vocal_I]
	inc bx 
	mov [vocal_I], bx

	cmp edi,lenArreglo ;cuando edi es menor
	jb leerArreglo

	jmp imprimirResultado	
letra_O:
	mov bx,[vocal_O]
	inc bx 
	mov [vocal_O], bx

	cmp edi,lenArreglo ;cuando edi es menor
	jb leerArreglo

	jmp imprimirResultado	
letra_U:    
	mov bx,[vocal_U]
	inc bx 
	mov [vocal_U], bx

	cmp edi,lenArreglo ;cuando edi es menor
	jb leerArreglo

	jmp imprimirResultado	


imprimirResultado:
	imprimir vocales, len_vocales

    ;imprimir salto,lenSalto

	mov bx,[vocal_A]
	mov [arreglo+0], bx
	cmp bx,1
	je Presentar_A

	add bx, '0'
	mov [vocal_A], bx
	;imprimir vocal_A, 5
	

	mov bx,[vocal_E]
	mov [arreglo+1], bx
	cmp bx,1
	je Presentar_B
	add bx, '0'
	mov [vocal_E], bx
	;imprimir vocal_E, 5

	mov bx,[vocal_I]
	mov [arreglo+2], bx
	cmp bx,1
	je Presentar_C
	add bx, '0'
	mov [vocal_I], bx
	;imprimir vocal_I, 5

	mov bx,[vocal_O]
	mov [arreglo+3], bx
	cmp bx,1
	je Presentar_D
	add bx, '0'
	mov [vocal_O], bx
	;imprimir vocal_O, 5

	mov bx,[vocal_U]
	mov [arreglo+4], bx
	cmp bx,1
	je Presentar_E
	add bx, '0'
	mov [vocal_U], bx
	;imprimir vocal_U, 5
	


	jmp salir
	
    
	;ORDENACION

Presentar_A:
	imprimir msg, len

	mov al, 49	
	add al,'0'
	mov [textoGuardar+0],al
	jmp guardarEnArchivo
Presentar_B:
	imprimir msg_2, len_2	

	mov al, 53
	add al,'0'
	mov [textoGuardar+0],al
	jmp guardarEnArchivo

Presentar_C:
	imprimir msg_3, len_3
	mov al, 57
	add al,'0'
	mov [textoGuardar+0],al
	jmp guardarEnArchivo
Presentar_D:
	imprimir msg_4, len_4
	mov al, 63
	add al,'0'
	mov [textoGuardar+0],al
	jmp guardarEnArchivo
Presentar_E:
	imprimir msg_5, len_5	
	mov al, 69
	add al,'0'
	mov [textoGuardar+0],al		
	jmp guardarEnArchivo
	;mov bx,[arreglo+0]

	;mov bx,[arreglo+1]

	;mov bx,[arreglo+2]
	;mov bx,[arreglo+3]
	;mov bx,[arreglo+4]


	;***********************************************
;************************ESCRIBIR EN ARCHIVO****


guardarEnArchivo:

	;* apertura del archivo ***
	mov eax,8 		;servicio para abrir el archivo/ tambien id del archivo ;5 PARA ARCHIVO EXISTENTE; 8 PARA CREAR NUEVO
	mov ebx,path2 	;direccion del archivo
	mov ecx,1	 		;modo de acceso solo lectura
	mov edx,0 		;permisos del archivo
	int 80H 		;igual a int 80H

	test eax,eax ; es igual que el and pero solo modifica las banderas no guarda en eax
	jz salir

	;* archivo sin exepciones **
	mov dword [id_archivo2],eax ; respaldamos el id del archivo

	
	mov eax,4
	mov ebx,[id_archivo2]
	mov ecx,textoGuardar 
	mov edx,1
	int 80H

	;* cerrar el archivo ***
	mov eax,6		 		;servicio para abrir el archivo
	mov ebx,[id_archivo2] 	;direccion del archivo
	mov ecx,0 				;modo de acceso solo lectura
	mov edx,0 				;permisos del archivo
	int 80H 				;igual a int 80H


salir:
    imprimir salto,lenSalto

	mov eax, 1
	int 80H
