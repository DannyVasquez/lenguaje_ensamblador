%macro imprimir 2
	mov eax,4
	mov ebx,1
	mov ecx,%1 
	mov edx,%2
	int 80H	
%endmacro

%macro leer 2
    mov eax,3
    mov ebx,0
    mov ecx,%1 
    mov edx,%2
    int 80H 
%endmacro

section .bss
	text resb 30
	text1 resb 30

	idarchivo resd 1
	idarchivo1 resd 1
    idarchivo2 resd 1

    resultado resb 10

section .data
	msj1 db "Primer Dato: "
	lenMsg1 equ $- msj1

	msj2 db "Segundo Dato: "
	lenMsg2 equ $- msj2

	msg db 'la resta es: ',10
	len equ $- msg

	salto db '',10
	lenSalto equ $- salto

	path1 db "/home/dannyvasquez/Escritorio/Ejercicios_ensamblador/ExamenArchivos/dato_1.txt",0 ; cero para cerrar el archivo
	path2 db "/home/dannyvasquez/Escritorio/Ejercicios_ensamblador/ExamenArchivos/dato_2.txt",0 ; cero para cerrar el archivo
    path3 db "/home/dannyvasquez/Escritorio/Ejercicios_ensamblador/ExamenArchivos/dato_3.txt",0 ; cero para cerrar el archivo

section .text
    global _start    

_start:
	;********* apertura del archivo *******************
	mov eax,5 		;servicio para abrir el archivo/ tambien id del archivo
	mov ebx,path1 	;direccion del archivo
	mov ecx,0 		;modo de acceso solo lectura
	mov edx,0 		;permisos del archivo
	int 80H 		;igual a int 80H

	test eax,eax ; es igual que el and pero solo modifica las banderas no guarda en eax
	jz salir

	;********* archivo sin exepciones ****************
	mov dword [idarchivo],eax ; respaldamos el id del archivo
	imprimir msj1 ,lenMsg1
	
	mov eax,3
	mov ebx,[idarchivo]
	mov ecx,text
	mov edx,30
	int 80H

	imprimir text,5
	;imprimir salto,lenSalto


	;********* cerrar el archivo *****************
	mov eax,6		 		;servicio para abrir el archivo
	mov ebx,[idarchivo] 	;direccion del archivo
	mov ecx,0 				;modo de acceso solo lectura
	mov edx,0 				;permisos del archivo
	int 80H 				;igual a int 80H


    ;****************************SEGUNDO ARCHIVO************************************


    ;********* apertura del archivo *******************
	mov eax,5 		;servicio para abrir el archivo/ tambien id del archivo
	mov ebx,path2 	;direccion del archivo
	mov ecx,0 		;modo de acceso solo lectura
	mov edx,0 		;permisos del archivo
	int 80H 		;igual a int 80H

	test eax,eax ; es igual que el and pero solo modifica las banderas no guarda en eax
	jz salir

	;********* archivo sin exepciones ****************
	mov dword [idarchivo1],eax ; respaldamos el id del archivo
	imprimir msj2 ,lenMsg2
	
	mov eax,3
	mov ebx,[idarchivo1]
	mov ecx,text1
	mov edx,30
	int 80H

	imprimir text1,5
	imprimir salto,lenSalto

  
	


	;********* cerrar el archivo *******************
	mov eax,6		 		;servicio para abrir el archivo
	mov ebx,[idarchivo1] 	;direccion del archivo
	mov ecx,0 				;modo de acceso solo lectura
	mov edx,0 				;permisos del archivo
	int 80H 				;igual a int 80H



;*********************************************
;*************************************************OPERACION
    
    
    ;PONGO EL VALOR EN UNA VARIABLE
    mov eax,[text]
    sub eax, '0'
    ;mov [text], eax

    ;PONGO EL VALOR EN UNA VARIABLE
    mov ebx,[text1]
    sub ebx, '0'
    ;mov [text1], eax

    
    ;mov eax,[text]
    ;mov ebx,[text1]
    add eax, ebx

    add eax, '0'
    mov [resultado], eax
    imprimir resultado,5

    
;***********************************************
;************************ESCRIBIR EN ARCHIVO****


guardarEnArchivo:

	;* apertura del archivo ***
	mov eax,5 		;servicio para abrir el archivo/ tambien id del archivo ;5 PARA ARCHIVO EXISTENTE; 8 PARA CREAR NUEVO
	mov ebx,path3 	;direccion del archivo
	mov ecx,1	 		;modo de acceso solo lectura
	mov edx,0 		;permisos del archivo
	int 80H 		;igual a int 80H

	test eax,eax ; es igual que el and pero solo modifica las banderas no guarda en eax
	jz salir

	;* archivo sin exepciones **
	mov dword [idarchivo2],eax ; respaldamos el id del archivo

	
	mov eax,4
	mov ebx,[idarchivo2]
	mov ecx,resultado 
	mov edx,1
	int 80H

	;* cerrar el archivo ***
	mov eax,6		 		;servicio para abrir el archivo
	mov ebx,[idarchivo2] 	;direccion del archivo
	mov ecx,0 				;modo de acceso solo lectura
	mov edx,0 				;permisos del archivo
	int 80H 				;igual a int 80H


salir:
    imprimir salto,lenSalto

	mov eax, 1
	int 80H




	;*********************


	;***********************************************
;************************ESCRIBIR EN ARCHIVO****


guardarEnArchivo:

	;* apertura del archivo ***
	mov eax,5 		;servicio para abrir el archivo/ tambien id del archivo ;5 PARA ARCHIVO EXISTENTE; 8 PARA CREAR NUEVO
	mov ebx,path2 	;direccion del archivo
	mov ecx,1	 		;modo de acceso solo lectura
	mov edx,0 		;permisos del archivo
	int 80H 		;igual a int 80H

	test eax,eax ; es igual que el and pero solo modifica las banderas no guarda en eax
	jz salir

	;* archivo sin exepciones **
	mov dword [idarchivo2],eax ; respaldamos el id del archivo

	
	mov eax,4
	mov ebx,[idarchivo2]
	mov ecx,resultado 
	mov edx,1
	int 80H

	;* cerrar el archivo ***
	mov eax,6		 		;servicio para abrir el archivo
	mov ebx,[idarchivo2] 	;direccion del archivo
	mov ecx,0 				;modo de acceso solo lectura
	mov edx,0 				;permisos del archivo
	int 80H 				;igual a int 80H
