%macro presentar 2
	mov eax,4
	mov ebx,1
	mov ecx,%1 
	mov edx,%2
	int 80H	
%endmacro

%macro leer 2
    mov eax,3
    mov ebx,0
    mov ecx,%1 
    mov edx,%2
    int 80H 
%endmacro

section .bss

	arrelorecogido resb 30

	idarchivo resd 1
	idarchivo2 resd 1

    res resb 2
    contador_a resb 2
    contador_b resb 2
    
	numMayor resb 2

section .data


	msg2 db 'Los valores ordenados son:',10
	lenMsj2 equ $-msg2

	msj1 db "LEER EL ARCHIVO",10
	lenMsg1 equ $- msj1

	mensa_num_Mayor db "LEER EL ARCHIVO",10
	len_mensa_num_Mayor equ $- mensa_num_Mayor
	
     arreglo db 0,0,0,0,0,0,0,0,0
	lenArreglo equ $- arreglo

      nueva_linea db '',10
    len_nueva equ $-nueva_linea	

	path1 db "/home/dannyvasquez/Escritorio/Ejercicios_ensamblador/ExamenArchivos/NumerosDesordenados.txt",0 ; cero para cerrar el archivo
	path3 db "/home/dannyvasquez/Escritorio/Ejercicios_ensamblador/ExamenArchivos/ResultadoOrdenados.txt",0 ; cero para cerrar el archivo
    
    textoGuardar db "000000000"
	lenTextoGuardar equ $-textoGuardar


section .text
    global _start    

_start:
	
;* apertura del archivo ***
	mov eax,5 		;servicio para abrir el archivo/ tambien id del archivo
	mov ebx,path1 	;direccion del archivo
	mov ecx,0 		;modo de acceso solo lectura
	mov edx,0 		;permisos del archivo
	int 80H 		;igual a int 80H

	test eax,eax ; es igual que el and pero solo modifica las banderas no guarda en eax
	jz salir

	;* archivo sin exepciones **
	mov dword [idarchivo],eax ; respaldamos el id del archivo
	;imprimir msj1 ,lenMsg1
	
	mov eax,3
	mov ebx,[idarchivo]
	mov ecx,arrelorecogido
	mov edx,30
	int 80H

     presentar nueva_linea,len_nueva
     presentar msj1,lenMsg1
    presentar nueva_linea,len_nueva
    presentar arrelorecogido,10
    presentar nueva_linea,len_nueva


	
	;* cerrar el archivo ***
	mov eax,6		 		;servicio para abrir el archivo
	mov ebx,[idarchivo] 	;direccion del archivo
	mov ecx,0 				;modo de acceso solo lectura
	mov edx,0 				;permisos del archivo
	int 80H 				;igual a int 80H


; *******************************
;*********************
   

	mov esi ,arreglo ;Indice inicial para recorrer el arreglo
	mov edi,0 ; el limite para recorrer.

	mov ecx,0


leerArreglo:
 
    ;para mover todos los valores leidos desde el archivo al arreglo
	mov al,[arrelorecogido+ecx]	
	sub al,'0'           ; le resto cero para guardarlos operables


	mov[esi],al
	inc esi
	inc edi
	inc ecx

	cmp edi,lenArreglo ;cuando edi es menor
	jl leerArreglo
		
	mov al,0


repetir1:

	mov [contador_a],al
	cmp al,lenArreglo
	je finalpresentar    ; salta cuando son iguales

	mov bx,0


repetir2:

	mov [contador_b],bx

	cmp bx,lenArreglo
	je salirbucle2

	mov cl,[arreglo+eax]
	mov dl,[arreglo+ebx]

	cmp dl,cl   ; compraro  2>1
	jg ordenartotal ; cuando el primero es mayor que el segundo
    

salirbucle:

	mov bx,[contador_b]
	mov al,[contador_a]; 

	inc bx

	jmp repetir2


salirbucle2:
	mov al,[contador_a]
	inc al
	jmp repetir1


finalpresentar:

    presentar nueva_linea,len_nueva
    presentar msg2,lenMsj2
    presentar nueva_linea,len_nueva

    mov ecx,0

imprimir:


	push rcx
	mov al,[arreglo+ecx]
	add al,'0'
	mov [res],al
    mov [textoGuardar+ecx],al
	
    presentar res,2  ;presentamos los valores del arreglo
	pop rcx
	inc ecx
	cmp ecx,lenArreglo
	jl imprimir   ;presentamos asta que termine el arreglo

	;*******************Numero MAYOR**************
	mov al, [arreglo+8]
	add al,'0'
	mov [numMayor],al
    

	presentar mensa_num_Mayor, len_mensa_num_Mayor
	presentar numMayor,2
    jmp salir



ordenartotal: 

	mov [arreglo+eax],dl
	mov [arreglo+ebx],cl

	jmp salirbucle


salir:

    ;********* apertura del archivo *******************
	mov eax,8		;servicio para abrir el archivo/ tambien id del archivo
	mov ebx,path3 	;direccion del archivo
	mov ecx,1	 		;modo de acceso solo lectura
	mov edx,0 		;permisos del archivo
	int 80H 		;igual a int 80H

	test eax,eax ; es igual que el and pero solo modifica las banderas no guarda en eax
	jz salir

	;********* archivo sin exepciones ****************
	mov dword [idarchivo2],eax ; respaldamos el id del archivo
	
	mov eax,4
	mov ebx,[idarchivo2]
	mov ecx,textoGuardar
	mov edx,lenTextoGuardar
	int 80H

	;********* cerrar el archivo *******************
	mov eax,6		 		;servicio para abrir el archivo
	mov ebx,[idarchivo2] 	;direccion del archivo
	mov ecx,0 				;modo de acceso solo lectura
	mov edx,0 				;permisos del archivo
	int 80H 				;igual a int 80H



    ;******************************





    presentar nueva_linea,len_nueva
    presentar textoGuardar,lenTextoGuardar
    presentar nueva_linea,len_nueva
	mov eax, 1
	mov ebx, 0
	int 80H