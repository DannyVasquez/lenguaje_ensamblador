section .data
       mensaje DB " MI PRIMERA VES CON NASM" ;constante mensaje de un byte en memoria
       longitud EQU $-mensaje                ;constante longitud que calcula el numero de caracteres



section .text
         global _start
_start:
        ;********************imprimir el mensaje***************************
        mov eax, 4          ;  por lo pronto  es un movimiento de datos al registro eax con la interrupcion el tipo de subrutina y pasa a ser de salida
        mov ebx, 1          ;  por lo pronto  es un movimiento de datos al registro ebx , (tipo de estandar por teclado)
        mov ecx, mensaje    ;  se alamacena la referencia a imprimir "mensaje"
        mov edx, longitud   ;  se alamacena la referencia a imprimir por numero de caracteres
        int 80H             ; interrupcion de sw para el sistema operativo linux

        mov eax, 1          ; salida cuando tenemos la interrupcion, salida del programa_exit, sys_exit
        mov ebx, 0          ; si el retorno es 0(200 en la web) ok      normalmente son movimientos pero cambia cuando le das una interrupcion
        int 80H
        
