section .data
    arreglo db 3,2,0,7,5
    len_arreglo equ $-arreglo

    nlinea		db		10,0
	lnlinea		equ		$ - nlinea


section .bss
    numero resb 1

section .text
    global _start    

_start:

    mov esi, arreglo    ;esi permite fijar el tamaño del arreglo, posicionar el arreglo en memoria
    mov edi,  0         ;edi va a contener el indice del arreglo    


    imprimir:
        
        mov al, [esi]   ;Tengo la memoria efectiva y accedo al arreglo el 1er valor
                        ;Se puede acceder a otro valor del arreglo con [esi+1], o [esi]+2   
        add al, '0'
        mov [numero] , al

        add esi, 1      ;Permite establecer como puntero base; y esta controlando.
        add edi, 1      ;[edi]
        

        mov eax, 4
        mov ebx, 1
        mov ecx, numero
        mov edx, 1 
        int 80H

        mov eax, 4
        mov ebx, 1
        mov ecx, nlinea
        mov edx, lnlinea 
        int 80H
        

        cmp edi, len_arreglo    ; cmp 3, 4 se activa carry, cuando el 1er operando es menor 
                                ; cmp 4, 3 se desactiva carry y zero
                                ; cmp 3, 3 se desactiva carry y zero se activa
                                
        jb imprimir             ; se ejecuta cuando la bandera de carry esta activada; SE activa ciando el 1er operando es menor q el  2do

    salir:
        mov eax,1
        int 0x80    