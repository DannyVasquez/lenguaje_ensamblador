%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

%macro leernum 2
     mov eax,3             ; define el tipo de operacion
     mov ebx,2              ;estandar de entrada
     mov ecx,%1            ;lo que capturo en el teclado
     mov edx,%2            ; numero de caracteres
     int 80h               ; interrupcion gnu linux
%endmacro




section	.data

msgfac db "El resultado del factorial es :"

len_msg equ $ - msgfac	

; ingrese un numero
ing db "Ingrese el numero para el factorial :"

len_msg_ing equ $ - ing



nueva_linea db 10,'',10
len_nueva equ $-nueva_linea	


section .bss

respuesta resb 5

; en caso pida por pantalla
num resb 5


section	.text


   global _start        
	
_start:                 
  

  
   imprimir ing,len_msg_ing

   leernum  num,5


   mov bl,[num]   
   sub bl, '0'     
   mov al,[num] 
   sub al, '0' 
   
   
   ;mov bx,3   ; para ser estatico
     
   ;mov ax,3   ; para ser estatico
  

   
   jmp  bucle_fac
        
bucle_fac:
   

   dec bx

   cmp bx,0
   je  presentar



   mul bx
   cmp   bx, 1
   jg    bucle_fac  ; salta cuando el bx>0
   


   jmp  presentar


presentar:


   add   ax, '0'

   mov  [respuesta], ax
    
   imprimir msgfac,len_msg

   imprimir nueva_linea,len_nueva

   imprimir respuesta,2
    

   imprimir nueva_linea,len_nueva


   mov	  eax,1        
   int	  80h
