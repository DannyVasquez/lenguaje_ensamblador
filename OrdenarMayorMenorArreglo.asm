;Realizado por Danny Vasquez

%macro imprimir 2
	mov eax,4
	mov ebx,1
	mov ecx,%1 
	mov edx,%2
	int 80H	
%endmacro

%macro leer 2
    mov eax,3
    mov ebx,0
    mov ecx,%1 
    mov edx,%2
    int 80H 
%endmacro

section .data
	mensaje_1 db 'Ingrese 5 valores: ',10
	lenMsj equ $-mensaje_1

	mensaje_3 db 10,'Los valores ya ordenados son:',10
	lenMsj2 equ $-mensaje_3

    nueva_linea db 10, ""

    arreglo db 0,0,0,0,0
    lenArreglo equ $-arreglo

section .bss    
    aux_1 resb 2
    aux_2 resb 2
    numero resb 2
section .text
    global _start
    
_start:
	mov esi ,arreglo ;esi permite fijar el tamaño del arreglo, posicionar el arreglo en memoria
	mov edi,0 ;edi va a contener el indice del arreglo  

	;Mensaje uno
	imprimir mensaje_1, lenMsj

leerValores:
	
	leer numero, 2

	mov al,[numero]	
	sub al,'0'
	mov[esi],al
	inc esi
	inc edi

	cmp edi,lenArreglo 
	jb leerValores	;Si el 1ero es MAYOR que el 2do
	mov al,0

Primer_Ciclo:
	mov [aux_1],al
	cmp al,lenArreglo
	je variable	;Si el 1ero es IGUAL que el 2do
	mov bx,0

Sub_ciclo:
	mov [aux_2],bx
	cmp bx,lenArreglo
	je Fin_Ciclo_2	;Si el 1ero es IGUAL que el 2do

	mov cl,[arreglo+eax]
	mov dl,[arreglo+ebx]

	cmp dl,cl
	ja Ordenacion_Valores ;Si el 1ero es IGUAL que el 2do ;-----------------------Para ambiar a mayor A mayor usar jb
Fin_Ciclo_1:
	mov bx,[aux_2]
	mov al,[aux_1]; recuperamos el valor de eax
	inc bx
	jmp Sub_ciclo

Fin_Ciclo_2:
	mov al,[aux_1]
	inc al
	jmp Primer_Ciclo

Ordenacion_Valores:
	mov [arreglo+eax],dl
	mov [arreglo+ebx],cl
	jmp Fin_Ciclo_1	
variable:
	imprimir mensaje_3,lenMsj2
	mov ecx,0
	jmp imprimir_Numero
imprimir_Numero:
	push rcx

	mov al,[arreglo+ecx]
	add al,'0'
	mov [numero],al
	
	imprimir numero,2

	pop rcx	
	inc ecx

	cmp ecx,lenArreglo
	jb imprimir_Numero	;Si el 1ero es MAYOR que el 2do

salir:
	mov eax, 1
	int 80H
