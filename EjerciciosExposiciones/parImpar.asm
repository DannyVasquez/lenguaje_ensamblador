section .data
        mensajePar db 'Numero par', 10
        len_mensajePar equ $-mensajePar

        mensajeImpar db 'Numero impar', 10
        len_mensajeImpar equ $-mensajeImpar
section .text
   global _start            
	
_start:                     
    mov   ax, 9  ;      9 --> 1001      1 --> 0001           
    and   ax, 1  ;      resultado ---> 0001            
    jz   par ;           
    jmp impar     


par:   
  
    mov   eax, 4             
    mov   ebx, 1             
    mov   ecx, mensajePar     
    mov   edx, len_mensajePar          
    int   80H
    jmp salir

impar:

    mov   eax, 4             
    mov   ebx, 1             
    mov   ecx, mensajeImpar       
    mov   edx, len_mensajeImpar          
    int   80H
    jmp salir                  
   
salir:
    mov eax, 1
    int 80H
