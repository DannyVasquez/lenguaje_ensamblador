%macro leer 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

%macro escribir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1 
    mov edx, %2
    int 80h
%endmacro

section .data

    msj0 db 10,"Ingrese un numero : ", 10
    len0 equ $-msj0
    msj7 db 10," "
    len7 equ $-msj7

section .bss
    n1 resb 2
    n2 resb 1


section .text
    global _start

_start:

    escribir msj0, len0
   	leer n1, 1  


ciclo:

    mov ax, [n1]       ; movimiento de n1  eax par decrementar
    push ax            ; recatamos el valor de eax debido al macro escribir 

    escribir n1, 1      ; escribimos el primer numero , forma decendente 
    escribir msj7, 1    ; salto de linea

    pop ax             ;recumeramos el eax 
    sub ax, '0'        ;caracter a numerico 

    dec ax             ;decrementa a eax
    mov cx, ax        ;movemos eax a ecx para poder realizar la comparacion


    add ax, '0'        ; numerico a caracter 
    mov [n1], ax       ;movimiento de eax al n1 pra poder presentar 

    cmp cx, 0
    jz salir            ; es igual a cero 
    jmp ciclo           ;




salir:				
    mov eax, 1
    int 80h



